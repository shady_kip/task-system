<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends ResponseController
{
    public function logout(Request $request)
    {

        $isUser = $request->user()->token()->revoke();
        if ($isUser) {
            $success['success'] = true;
            return $this->sendResponse($success);
        } else {
            $success['success'] = false;
            return $this->sendResponse($success);
        }
    }
}
