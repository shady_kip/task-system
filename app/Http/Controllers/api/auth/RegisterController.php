<?php

namespace App\Http\Controllers\api\auth;

use App\Http\Controllers\api\ResponseController;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends ResponseController
{
    public function createUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "emp_name" => "required|min:3|max:20",
            "department" => "required",
            "email" => "required|email|unique:users",
            "job_level" => "required|unique:users",
            "password" => "required|min:5"
        ]);
        if ($validator->fails()) {
            return $this->sendError([
                'validation' => 'failed',
                'error' => $validator->errors()
            ], 400);
        }
        $request->merge([
            "password" => Hash::make($request->password)
        ]);
        $user = new User();
        $user->emp_name = $request->emp_name;
        $user->email = $request->email;
        $user->job_level = $request->job_level;
        $user->emp_department = $request->department;
        $user->password = $request->password;
        if ($user->save()) {
            $token =  $user->createToken(env("APP_TOKEN"))->accessToken;
            return $this->sendResponse([
                "user" => $user,
                "token" => $token,
                "message" => "Registration successfull !"
            ]);
        } else {
            $error = [
                'error' => true,
                'message' => "Registration failed !"
            ];
            return $this->sendError($error, 401);
        }
    }
}
