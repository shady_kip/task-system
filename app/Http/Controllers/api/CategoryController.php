<?php

namespace App\Http\Controllers\api;

use App\Category;
use Illuminate\Http\Request;


class CategoryController extends ResponseController
{
    public function index(Request $request)
    {
        $categories = Category::all();
        return $categories;
    }
    public function create(Request $request)
    {
        $category = new Category();
        $category->name = $request->name;
        $category->department = $request->department;
        $category->user_id = auth()->user()->id;
        $cat = $category->save();
        if ($cat) {
            return $this->sendResponse([
                "category" => $cat,
                "success" => true,
                "message" => "Category added !"
            ]);
        }
    }

    public function update(Request $request, $slug)
    {
        $cat = Category::where('slug', $slug)->first();
        if ($cat) {
            $cat->name = $request->name;
            $cat->department = $request->department;
            $cat->save();
            return $this->sendResponse([
                "category" => $cat,
                "message" => "Category updated !"
            ]);
        }
    }

    public function delete($slug)
    {
        Category::where('slug', $slug)->delete();
        return $this->sendResponse([
            "message" => "Deleted !"
        ]);
    }
}
