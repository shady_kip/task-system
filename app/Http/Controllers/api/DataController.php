<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class DataController extends Controller
{
    public function fetchUsers(Request $request, $department)
    {
        $users = User::where('emp_department', 'LIKE', '%' . $department . '%')->get();
        return $users;
    }
}
