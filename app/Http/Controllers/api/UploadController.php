<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function upload(Request $request)
    {
        // $docs = $request->file('docs');
        // for ($i = 0; $i < count($docs); $i++) {
        //     $file = $docs[$i];
        //     // Get filename with extension
        //     $filenameWithExt = $file->getClientOriginalName();
        //     // Get file path
        //     $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //     // Remove unwanted characters
        //     $filename = preg_replace("/[^A-Za-z0-9 ]/", '', $filename);
        //     $filename = preg_replace("/\s+/", '-', $filename);
        //     // Get the original image extension
        //     $extension = $file->getClientOriginalExtension();

        //     // Create unique file name
        //     $fileNameToStore = $request->id . "_" . $filename . '_' . time() . '.' . $extension;

        //     Storage::put("public/docs/{$fileNameToStore}", $file);
        // }
        return response()->json([
            'success' => true,
            'docs' => ''
        ]);
    }
}
