import React, { useCallback, useContext, useEffect, useState } from "react";
import { useDropzone } from "react-dropzone";
import { AppContext } from "../store/AppContext";

const thumbsContainer = {
  display: 'flex',
  flexDirection: 'row',
  flexWrap: 'wrap',
  marginTop: 16
};

const thumb = {
  display: 'inline-flex',
  borderRadius: 2,
  border: '1px solid #eaeaea',
  marginBottom: 8,
  marginRight: 8,
  width: 100,
  height: 100,
  padding: 4,
  boxSizing: 'border-box'
};

const thumbInner = {
  display: 'flex',
  minWidth: 0,
  overflow: 'hidden',
  position:'relative'
};

const img = {
  display: 'block',
  width: 'auto',
  height: '100%'
};

const Uploader = () => {
    const [files, setFiles] = useState([]);
    const ctxt= useContext(AppContext)
    const onDrop = useCallback((acceptedFiles) => {
        setFiles(
            acceptedFiles.map((file) =>
                Object.assign(file, {
                    preview: URL.createObjectURL(file),
                })
            )
        );
          ctxt.DispatchAction({
            type:"docs",
            payload:{
                docs:acceptedFiles
            }

        })
     

    }, []);
  

    useEffect(() => {
        // Make sure to revoke the data uris to avoid memory leaks
        files.forEach((file) => URL.revokeObjectURL(file.preview));
    }, [files]);

    const { getRootProps, getInputProps, isDragActive } = useDropzone({
        onDrop,
    });
   
      const thumbs = files.map(file => {
     
          return (
                <div style={thumb} key={file.name}>
                <div  style={thumbInner}>
                    {
                        file.type==='image/jpeg' || file.type==='image/png' || file.type==='image/jpg' ? <img alt="img" src="/images/image.png"/>:''
                    }
                    {
                        file.type==='application/pdf'?<img alt="img" src="/images/pdf.png"/>:""
                    }
                    {
                        file.type==='application/vnd.openxmlformats-officedocument.wordprocessingml.document'?<img alt="img" src="/images/doc.png"/>:""
                    }
                    <span className="filename">{file.name}</span>
                </div>
                </div>
            )
      }
            );
    return (
        <div className="drop-container"  {...getRootProps()}>
            <div className="dropzone">
            <input {...getInputProps()} />
            {isDragActive ? (
                <p>Drop the files here ...</p>
            ) : (
                <p>Drag 'n' drop some files here, or click to select files</p>
            )}
            </div>
             <aside style={thumbsContainer}>
        {thumbs}
      </aside>
        </div>
    );
};

export default Uploader;
