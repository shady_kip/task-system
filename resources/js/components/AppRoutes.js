import React from 'react'
import { Link, Route, Routes } from 'react-router-dom'
import AddCategory from '../pages/AddCategory'
import AddTask from '../pages/AddTask'
import Login from '../pages/auth/Login'
import Register from '../pages/auth/Register'
import Category from '../pages/Category'
import Dashboard from '../pages/Dashboard'
import PageNotFound from '../pages/PageNotFound'
import Tasks from '../pages/Tasks'
import PrivateRoute from './PrivateRoutes'

export const AppRoutes = () => {

  const BrokenLink=()=>{
    return <h6>Broken link , go to <Link to={`/dashboard`}> dashboard </Link></h6>
  }
  return (
    <Routes>
        <Route path='/' element={<Login/>}/>
        <Route path='/register' element={<Register/>}/>
        <Route path='/dashboard' element={<PrivateRoute><Dashboard/></PrivateRoute>}>
           <Route path='add-task' element={<AddTask/>}/>
           <Route path='categories' element={<Category/>}>
             <Route path='add-category' element={<AddCategory/>}/>
              <Route path="*" element={<BrokenLink/>} />
           </Route>
           <Route path='tasks-followup' element={<Tasks/>}/>
           <Route path="*" element={<BrokenLink/>} />
        </Route>
        <Route path='*' element={<PageNotFound/>}/>
    </Routes>
  )
}
