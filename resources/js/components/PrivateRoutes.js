
import { Navigate } from 'react-router-dom';
import AccountService from '../services/AccountService';


import React from 'react'
  function PrivateRoute({children}) {
    const auth=  AccountService.checkAuth()
    return auth ? children : <Navigate to='/'/>
}

export default PrivateRoute
