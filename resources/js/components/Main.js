import React, { Component, useReducer } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import Layout from '../layouts/Layout';
import { AppRoutes } from './AppRoutes';
if (document.getElementById('app')) {
        
    ReactDOM.render(
        <BrowserRouter>
            <Layout>
              <AppRoutes/>
           </Layout>
        </BrowserRouter>
        , document.getElementById('app'));
}
