
import { toast } from 'react-toastify';

const Toastr = (msg,type) => {
  return   toast(msg,{
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            type
            })
}

export default Toastr