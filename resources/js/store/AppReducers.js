export default (state , { type, payload }) => {
    switch (type) {
    case "auth":
        return { ...state, ...payload }
    case "alertStatus":
        return { ...state, ...payload }
    case "docs":
        console.log(payload);
        return { ...state, ...payload }
    default:
        return state
    }
}
