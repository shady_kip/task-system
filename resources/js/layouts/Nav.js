import React, { useContext } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import AccountService from '../services/AccountService'
import { AppContext } from '../store/AppContext'

const Nav = () => {
  const ctxt=useContext(AppContext)
 const nav= useNavigate()
  console.log(ctxt);
  const logout=async()=>{
     if(window.confirm("logout?")){
       let resp = await AccountService.logout()
       if(resp.success){
         ctxt.DispatchAction({
           type:"auth",
           payload:{
             user:null
           }
         })
         nav("/")
       }
     }
  }
  return (
  <nav className="navbar navbar-dark fixed-top shadow-sm bg-white">
  <div className="container-fluid d-flex justify-content-between align-items-center">
      <span><Link className="logo" to={`/dashboard`}>Task Manager</Link></span>
      {
        ctxt.AppData.user ? <span><Link onClick={logout} className="navbar-brand" to={`#`}> Logout </Link></span> : <span><Link className="navbar-brand" to={`/`}> Login </Link></span>
      }
       
    </div>
</nav>
  )
}

export default Nav