import React, { useEffect, useReducer } from "react";
import Nav from "./Nav";
import Footer from "./Footer";
import { AppContext } from "../store/AppContext";
import AppReducers from "../store/AppReducers";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import AccountService from "../services/AccountService";

const Layout = (props) => {
    const initialState = {
        user: null,
        alert: false,
        docs:[]
    };
    const [AppData, dispatch] = useReducer(AppReducers, initialState);
    useEffect(() => {
        let status = AccountService.checkAuth();
        if (status) {
            let user = JSON.parse(localStorage.getItem("auth-user"));
            dispatch({
                type: "auth",
                payload: user,
            });
        }
    }, [status]);
    return (
        <>
            <AppContext.Provider
                value={{
                    AppData,
                    DispatchAction: dispatch,
                }}
            >
                <Nav />
                <main className="full-height nav-clearfix">{props.children}</main>
                <Footer />
            </AppContext.Provider>
            <ToastContainer />
        </>
    );
};

export default Layout;
