import Axios from "axios"

class AccountService{

    async createUser(user){
        let resp = await Axios.post(`/api/create-user`,user,{
            headers:{
                 "content-type":"application/json"
            }
        })
        return await resp.data;

    }

    async login(user){
         let resp = await Axios.post(`/api/login-user`,user,{
            headers:{
                 "content-type":"application/json"
            }
        })
        let data = await resp.data;
        this.saveUSer(data)
        return data;
    }
    
     saveUSer(user){
        localStorage.setItem('auth-user',JSON.stringify(user))
    }

    checkAuth(){
        let user = localStorage.getItem('auth-user')
        if(user){
            return true
        }else{
            return false
        }
    }

    async logout(){
          let resp= await Axios.get(`/api/logout`,{
              headers:{
                  'Authorization':`Bearer ${this.getToken()}`
              }
          });
          if(resp.data){
                   localStorage.removeItem('auth-user')
          }
       
          return resp.data;  
    }

    getToken(){
        let user = JSON.parse(localStorage.getItem('auth-user'))
        if(user){
            return user.token
        }
    }
}

export default new AccountService