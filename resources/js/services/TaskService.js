import Axios from "axios"
import AccountService from "./AccountService"

class TaskService{
    async addTask(data){
        let resp = await Axios.post(`/api/tasks/create`,data,{
            headers:{
                 "Authorization":`Bearer ${AccountService.getToken()}`
            }
        })
        return resp.data
    }

    async getTasks(){
           let resp = await Axios.get(`/api/tasks`,{
            headers:{
                 "Authorization":`Bearer ${AccountService.getToken()}`
            }
        })
        return resp.data
    }
}
export default new TaskService()