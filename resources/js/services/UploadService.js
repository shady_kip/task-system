import Axios from "axios";

class UploadService{

    async uploadDoc(data,id){
        let formData=new FormData();
        formData.append('id',id)
        console.log('documents :',data)
        for (let i = 0; i < data.length; i++) {
            formData.append('docs[]',data[i])
        }
        
        let resp = await Axios.post(`/api/tasks/docs/create`,formData,{
            headers:{
                 "Authorization":`Bearer ${AccountService.getToken()}`,
            }
        })
         let data_ = await resp.data;

        console.log(data_)
       
    }
}

export default new UploadService()