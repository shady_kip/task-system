import Axios from "axios";
import AccountService from "./AccountService"
class CategoryService{
  
    CategoryService(){
        
    }
    async addCategory(data){
   
        let resp = await Axios.post(`/api/categories/create`,data,{
            headers:{
                 "Authorization":`Bearer ${AccountService.getToken()}`
            }
        })
        return resp.data
    }

    async getCategories(){
            let resp = await Axios.get(`/api/categories`,{
            headers:{
                 "Authorization":`Bearer ${AccountService.getToken()}`
            }
        })
        return resp.data
    }
    
}

export default new CategoryService()