import Axios from "axios"
import AccountService from "./AccountService"

class DataService{

    async fetchDepartmentMembers(department){
        let resp  = await Axios.get(`/api/members/${department}`,{
            headers:{
                 "Authorization":`Bearer ${AccountService.getToken()}`
            }
        })
        return  resp.data
        
    }
}
export default new DataService()