import { ChevronRightOutlined } from "@material-ui/icons";
import React from "react";
import { useNavigate } from "react-router-dom";

const Sidebar = () => {
    const navigate = useNavigate();
    const menuItems = [
        {
            title: "Dashboard",
            slug: "dashboard",
        },
        {
            title: "Task Categories",
            slug: "categories",
        },
        {
            title: "Tasks & Follow Up",
            slug: "tasks-followup",
        },
        {
            title: "Project",
            slug: "project",
        },
        {
            title: "My Projects",
            slug: "my-project",
        },
        {
            title: "Issues",
            slug: "issues",
        },
        {
            title: "User Board",
            slug: "user-board",
        },

    ];

    const nav = (slug) => {
       if(slug==='dashboard'){
         navigate(`/${slug}`);
       }else{
         navigate(`/dashboard/${slug}`);
       }
        
    };
    return (
        <>
            <div className="sidebar-links">
                <div className="text-center mt-3">
                    <h4>Menu</h4>
                </div>
                {menuItems.map((menu, i) => {
                    return (
                        <div
                            onClick={() => nav(menu.slug)}
                            key={i}
                            className="sidebar-link"
                        >
                            <span>{menu.title}</span>
                            <span>
                                <ChevronRightOutlined />
                            </span>
                        </div>
                    );
                })}
            </div>
        </>
    );
};

export default Sidebar;
