import React from 'react'

const PageNotFound = () => {
  return (
    <div className='full-height  flex-center'>
    <h1>404 | Not Found </h1>
    </div>
  )
}

export default PageNotFound