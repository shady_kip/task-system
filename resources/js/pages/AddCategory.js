import { ChevronLeft } from "@material-ui/icons";
import { Formik } from "formik";
import React, { useContext } from "react";
import { useNavigate } from "react-router-dom";
import { string } from "yup";
import { object } from "yup";
import Toastr from "../helpers/Toastr";
import CategoryService from "../services/CategoryService";
import { AppContext } from "../store/AppContext";

const formSchema = object({
    name: string().required("Required"),
});
const AddCategory = () => {
    const nav = useNavigate();
    const ctxt = useContext(AppContext);
   
    
    return (
        <>
            <div className="title-header">
                <span>
                    <b>
                        <h5>Add Category</h5>
                    </b>
                </span>
                <span>
                    <button
                        onClick={() => nav("/dashboard/categories")}
                        className="btn btn-theme btn-rounded"
                    >
                        Go back <ChevronLeft />{" "}
                    </button>
                </span>
            </div>

            <div>
                <Formik
                    initialValues={{
                        name: "",
                        department: "",
                    }}
                    validationSchema={formSchema}
                    onSubmit={async(data,{resetForm}) => {
                        if(ctxt.AppData.user!==null){
                         data.department=ctxt.AppData.user.emp_department;
                         let resp=await CategoryService.addCategory(data)
                         console.log(resp)
                         if(resp.success){
                             resetForm()
                            return Toastr(resp.message,'success')
                         }
                        }
                    }}
                >
                    {({
                        handleSubmit,
                        handleChange,
                        handleBlur,
                        values,
                        touched,
                        errors,
                        isSubmitting,
                    }) => {
                        return (
                            <form onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <label htmlFor="name">Category name</label>
                                    <input
                                        name="name"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.name}
                                        className="form-control"
                                    />
                                    {errors.name && touched.name && (
                                        <p className="text-danger">
                                            {errors.name}
                                        </p>
                                    )}
                                </div>
                          <div className="form-group">
                                    <label htmlFor="department">Department</label>
                                    <input
                                        name="name"
                                        readOnly
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={ctxt.AppData.user!==null ? ctxt.AppData.user.emp_department:``}
                                        className="form-control"
                                    />
                                    {errors.department && touched.department && (
                                        <p className="text-danger">
                                            {errors.department}
                                        </p>
                                    )}
                                </div>
                                <div className="form-group">
                                    <button disabled={isSubmitting} className="btn w-100 btn-theme">
                                        {
                                            isSubmitting?
                                            "Saving..."
                                            :
                                            "Save"
                                        }
                                    </button>
                                </div>
                            </form>
                        );
                    }}
                </Formik>
            </div>
        </>
    );
};

export default AddCategory;
