import { AddCircle, Delete, Edit } from '@material-ui/icons'
import React, { useEffect, useState } from 'react'
import { Link, useNavigate } from 'react-router-dom'
import DataTable from 'react-data-table-component';
import ReactHtmlParser, {
    processNodes,
    convertNodeToElement,
    htmlparser2,
} from "react-html-parser";
import TaskService from '../services/TaskService';

const Tasks = () => {
    const navigate=useNavigate()
      const [data,setData]=useState([])
    useEffect(()=>{
    const fetchData=async()=>{
       let resp=   await TaskService.getTasks()
       setData(resp)
      }
      fetchData()
    },[])

    const columns = [
    {
        name: 'Description',
        selector: row => <p>{ReactHtmlParser(row.description)}</p>,
    },
    {
        name: 'Department',
        selector: row => row.department,
    },
        {
        name: 'Action',
        selector: row => (
            <>
            <span><Link title='Edit' to={`/dashboard/tasks/edit-tasks/${row.id}`}>
                <Edit/>
                </Link></span>
             
            <span><Link className='text-danger' title='delete' to={`/dashboard/tasks/delete-task/${row.id}`}>
                <Delete/>
                </Link></span>


            </>
        ),
    },
];

  return (
    <>
       <div>
        <div className='title-header'>
            <span>
                <b><h5>All Tasks</h5></b>
            </span>
            <span>
                <button onClick={()=>navigate('/dashboard/add-task')} className='btn btn-theme btn-rounded'>Add Task <AddCircle/> </button>
            </span>
        </div>
     
    </div>

  <DataTable
            columns={columns}
            data={data}
            selectableRows
            pagination
            
        />
    </>
  )
}

export default Tasks