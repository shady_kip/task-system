import React, { useState } from 'react'
import { Outlet, useLocation } from 'react-router-dom'
import Sidebar from './includes/Sidebar'
import Tasks from './Tasks';


const Dashboard = () => {
  const location = useLocation();
  let strArr=[]
  if(location.pathname!==undefined){
   let path = location.pathname;
   strArr=path.split('/')
  
  }
   const Dash = ()=>{
      return(
    <div><Tasks/></div>
  )
   }
  
  return (
    <div className='container-fluid' >
      <div className='dash-container mt-3'>
        <div className='sidebar shadow-sm'>
          <Sidebar/>
        </div>
        <div className='dash-content shadow-sm'>
          {
            strArr.length==2 && strArr[1]==='dashboard'?<Dash/>:<Outlet/>
          }
        </div>
      </div>
    </div>
  )
}



export default Dashboard