import { AttachFile, ChevronLeftOutlined } from "@material-ui/icons";
import { Formik } from "formik";
import React, { useContext, useEffect, useState } from "react";
import { object } from "yup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { string } from "yup";
import ReactQuill from "react-quill";
import "react-quill/dist/quill.snow.css";
import Select from "react-select";
import DataService from "../services/DataService";
import CategoryService from "../services/CategoryService";
import Uploader from "../components/Uploader";
import { AppContext } from "../store/AppContext";
import TaskService from "../services/TaskService";
import Toastr from "../helpers/Toastr";
import UploadService from "../services/UploadService";
import { useNavigate } from "react-router-dom";

const formSchema = object({
    access: string().required("Access level is required"),
   // assigned_to: string().required("Required"),
    due_date: string().required("Required"),
    priority: string().required("Required"),
});
const AddTask = () => {
    const [startDate, setStartDate] = useState(new Date());
    const [members, setMembers] = useState([]);
    const [cats, setCats] = useState([]);
    const [deprt, setDeprt] = useState("");
     const [catname, setCatname] = useState("");
     const [assignees, setAssignees] = useState([]);
     const [desc, setDesc] = useState("");
    const [loading, setLoading] = useState(false);
    const ctxt = useContext(AppContext)
    const nav = useNavigate()

    useEffect(() => {
        const fetchData = async () => {
            let catArr=[]
            let resp = await CategoryService.getCategories();
            resp.map((item,i)=>{
                  let Obj = {
                value: null,
                label: "",
            };
            Obj.value = item.name;
            Obj.label = item.name;
            catArr.push(Obj)
            })
            setCats(catArr)
        };
        fetchData();
    }, []);

    const fetchMembers = async (e) => {
        let val = e.target.value;
        setLoading(true);
        let updatedArr = [];
        setDeprt(val);
        let data = await DataService.fetchDepartmentMembers(val);
        if (data) {
            setLoading(false);
        }
        data.forEach((item, i) => {
            let usrObj = {
                value: null,
                label: "",
            };
            usrObj.value = item.id;
            usrObj.label = item.emp_name;
            updatedArr.push(usrObj);
        });
        setMembers(updatedArr);
    };

    const handleSelection = (opt) => {
        setCatname(opt.value)
    
    };
    const handleMultiSelect=(opt)=>{
        setAssignees(opt)
    }
    const handleDesc=(val)=>{
        setDesc(val)
    }



    return (
        <div>
            <div className="title-header">
                <span>
                    <button onClick={()=>nav('/dashboard/tasks-followup')} className="btn">
                        <ChevronLeftOutlined /> Back
                    </button>
                </span>
                <span>New Task</span>
            </div>
            <Formik
                initialValues={{
                    category: "",
                    department: "",
                    due_date: new Date(),
                    access: "",
                    assigned_to: "",
                    priority: "",
                    description:"",
                    docs:[]
                }}
                validationSchema={formSchema}
                onSubmit={async(data) => {
                
                    data.category=catname;
                    data.assigned_to=assignees;
                    data.department=deprt;
                    data.docs=ctxt.AppData.docs
                    data.description=desc
                    data.due_date=startDate
                    let resp=await TaskService.addTask(data);
                    if(resp.success){
                        if(data.docs.length>0){
                            let rsp=await UploadService.uploadDoc(data.docs,resp.task.id);
                            console.log(rsp);
                        }else{
                        return Toastr('Task added !','success')
                        }
                    }                    
                }}
            >
                {({
                    handleBlur,
                    handleSubmit,
                    errors,
                    values,
                    touched,
                    handleChange,
                    isSubmitting,
                }) => {
                    return (
                        <form onSubmit={handleSubmit}>
                            <div className="row">
                                <div className="col-6">
                                    <label>Task Category</label>
                                     <Select
                                        isLoading={loading}
                                        options={cats}
                                        onChange={handleSelection}
                                    />
                                   
                                    {errors.category && touched.category && (
                                        <p className="text-danger">
                                            {errors.category}
                                        </p>
                                    )}
                                </div>
                                <div className="col-6">
                                    <label>Due date</label>
                                    <DatePicker
                                        required
                                        className="form-control"
                                        selected={startDate}
                                        onChange={(date) => setStartDate(date)}
                                        onBlur={handleBlur}
                                        value={startDate}
                                    />
                                    {errors.due_date && touched.due_date && (
                                        <p className="text-danger">
                                            {errors.due_date}
                                        </p>
                                    )}
                                </div>

                                <div className="col-6">
                                    <label>Priority</label>
                                    <select
                                        name="priority"
                                        className="form-control"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.priority}
                                    >
                                        <option defaultValue={``}>
                                            --select priority--
                                        </option>
                                        <option value={`High`}>High</option>
                                        <option value={`Medium`}>Medium</option>
                                        <option value={`Low`}>Low</option>
                                    </select>
                                    {errors.priority && touched.category && (
                                        <p className="text-danger">
                                            {errors.category}
                                        </p>
                                    )}
                                </div>
                                <div className="col-6">
                                    <label>Access Level</label>
                                    <select
                                        name="access"
                                        className="form-control"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.access}
                                    >
                                        <option defaultValue={``}>
                                            --select level--
                                        </option>
                                        <option value={`private`}>
                                            Private
                                        </option>
                                        <option value={`public`}>Public</option>
                                    </select>
                                    {errors.access && touched.access && (
                                        <p className="text-danger">
                                            {errors.access}
                                        </p>
                                    )}
                                </div>

                                <div className="col-6">
                                    <label>Department</label>
                                    <select
                                        name="department"
                                        className="form-control"
                                        onChange={fetchMembers}
                                        onBlur={handleBlur}
                                        value={deprt}
                                    >
                                        <option defaultValue={``}>
                                            --select department--
                                        </option>
                                        <option value={`IT department`}>
                                            IT department
                                        </option>
                                    </select>
                                    {errors.department &&
                                        touched.department && (
                                            <p className="text-danger">
                                                {errors.department}
                                            </p>
                                        )}
                                </div>
                                <div className="col-6">
                                    <label>Assigned to</label>
                                    <Select
                                        isLoading={loading}
                                        options={members}
                                        isMulti={true}
                                        onChange={handleMultiSelect}
                                    />
                                    {/* <select
                                        name="assigned_to"
                                        className="form-control"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.assigned_to}
                                    >
                                        <option defaultValue={``}>
                                            --select user--
                                        </option>
                                        <option value={`private`}>
                                            User 1
                                        </option>
                                        <option value={`public`}>User 2</option>
                                    </select> */}

                                    {errors.assigned_to &&
                                        touched.assigned_to && (
                                            <p className="text-danger">
                                                {errors.assigned_to}
                                            </p>
                                        )}
                                </div>

                                <div className="col-12 mt-3">
                                    <label><AttachFile/> Attach document(s) </label>
                                    <Uploader/>
                                </div>
                            </div>
                            <div className="mt-3 mb-3">
                                <label>Description</label>
                                <ReactQuill  onChange={handleDesc} theme="snow" />
                            </div>

                            <button disabled={isSubmitting} className="btn-theme mt-3 btn w-100">
                                {
                                    isSubmitting?'Saving...':"Submit"
                                }
                            </button>
                        </form>
                    );
                }}
            </Formik>
        </div>
    );
};

export default AddTask;
