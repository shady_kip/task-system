import { AddCircle, DeleteForever, Edit } from '@material-ui/icons'
import React, { useEffect, useState } from "react";
import { Link, Outlet, useLocation, useNavigate } from 'react-router-dom'
import CategoryService from '../services/CategoryService';
import DataTable from 'react-data-table-component';


const Category = () => {
  const nav=  useNavigate()
  const location=useLocation()
  const [data,setData]=useState([])
    let strArr=[]
  if(location.pathname!==undefined){
   let path = location.pathname;
   strArr=path.split('/')
  }
const columns = [
    {
        name: 'Name',
        selector: row => row.name,
    },
    {
        name: 'Department',
        selector: row => row.department,
    },
        {
        name: 'Action',
        selector: row => (
            <>
            <span><Link title='Edit' to={`/dashboard/categories/edit-category/${row.slug}`}>
                <Edit/>
                </Link></span>
            <span><Link className='text-danger' title='delete' to={`/dashboard/categories/delete-category/${row.slug}`}>
                <DeleteForever/>
                </Link></span>
            </>
        ),
    },
];

    useEffect(()=>{
      const fetchData=async()=>{
       let resp=   await CategoryService.getCategories()
       setData(resp)
      }
      fetchData()
    },[])

  const AllCategories=()=>{
      return (
          <>
          <div className='title-header'>
            <span>
                <b><h5>Categories</h5></b>
            </span>
            <span>
                <button onClick={()=>nav('/dashboard/categories/add-category')} className='btn btn-theme btn-rounded'>Add Category <AddCircle/> </button>
            </span>
        </div>
        <DataTable
            columns={columns}
            data={data}
            selectableRows
            pagination
            
        />
        </>
          
      )
  }
  return (
        <div>
        {
            strArr[2]==="categories" && strArr.length==3
            ?
            <AllCategories/>
            :
            <Outlet/>
        }
     
    </div>
  )
}

export default Category