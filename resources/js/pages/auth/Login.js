import { ArrowForward } from "@material-ui/icons";
import { Formik } from "formik";
import React, { useContext } from "react";
import { Link, useNavigate } from "react-router-dom";
import { object, string, ref } from "yup";
import { toast } from 'react-toastify';
import AccountService from "../../services/AccountService";
import { AppContext } from "../../store/AppContext";

const formSchema=object({
      email: string()
        .email("A valid email is required")
        .required("Email is required")
        .max(30)
        .min(3),
       password: string().required("Password is required").min(5),
})

const Login = () => {
 const nav= useNavigate()
 const ctxt= useContext(AppContext);
      const showToast=(msg,type)=>{
        return toast(msg,{
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            type
            })
    }
    return (
        <div className="full-height flex-center">
            <div className="auth-box shadow-sm">
                <div className="auth-card padding h-inherit">
                    <Formik
                    initialValues={{
                      email:"",
                      password:""
                    }}
                    validationSchema={formSchema}
                       onSubmit={async (data,{resetForm}) => {
                            console.log(data)
                            let resp = await AccountService.login(data)
                            if(resp.user){
                              showToast("Login Success",'success')
                              ctxt.DispatchAction({
                                type:"auth",
                                payload:resp
                              })
                              resetForm()
                              nav('/dashboard')
                            }else{
                                 showToast(resp.message,'error')
                            }
                            
                        }}
                    >
                        {({
                            handleSubmit,
                            handleChange,
                            handleBlur,
                            values,
                            touched,
                            errors,
                            isSubmitting,
                        }) => {
                            return (
                                <form onSubmit={handleSubmit}> 
                                    <div className="row">
                                        <div className="col-12 mb-3">
                                            <h4>Login</h4>
                                        </div>
                                        <div className="col-12 form-group">
                                            <label htmlFor="email">
                                                <b>Email Address</b>
                                            </label>
                                            <input
                                                type={`email`}
                                                className="form-control"
                                                 name="email"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.email}
                                            />
                                            {errors.email &&
                                                touched.email && (
                                                    <p className="text-danger">
                                                        {errors.email}
                                                    </p>
                                                )}
                                        </div>
                                        <div className="col-12 form-group">
                                            <label htmlFor="password">
                                                <b>Password</b>
                                            </label>
                                            <input
                                                type={`password`}
                                                className="form-control"
                                                name="password"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.password}
                                            />
                                            {errors.password &&
                                                touched.password && (
                                                    <p className="text-danger">
                                                        {errors.password}
                                                    </p>
                                                )}
                                        </div>
                                        <div className="col-12 form-group">
                                            <button
                                            disabled={isSubmitting}
                                                type="submit"
                                                className="btn btn-primary w-100"
                                            >
                                                Sign In
                                            </button>
                                        </div>
                                        <div className="col-12 form-group">
                                            <span></span> Register{" "}
                                            <Link to={`/register`}>
                                                Here <ArrowForward />{" "}
                                            </Link>
                                        </div>
                                    </div>
                                </form>
                            );
                        }}
                    </Formik>
                </div>
            </div>
        </div>
    );
};

export default Login;
