import { ArrowForward } from "@material-ui/icons";
import { Formik } from "formik";
import React from "react";
import { Link, useNavigate } from "react-router-dom";
import { object, string, ref } from "yup";
import AccountService from "../../services/AccountService";
import { toast } from 'react-toastify';

const formSchema = object({
    email: string()
        .email("A valid email is required")
        .required("Email is required")
        .max(30)
        .min(3),
    emp_name: string().required("Name is required").min(3).max(20),
    job_level: string().required("Job level is required"),
    department: string().required("Department is required"),
    password: string().required("Password is required").min(5),
});
const Register = () => {
  const nav=  useNavigate()
    const showToast=(msg,type)=>{
        return toast(msg,{
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
            type
            })
    }
    return (
        <div className="full-height flex-center">
            <div className="auth-box shadow-sm">
                <div className="auth-card padding h-inherit">
                    <Formik
                        initialValues={{
                            email: "",
                            emp_name: "",
                            department: "",
                            job_level: "",
                            password: "",
                        }}
                        validationSchema={formSchema}
                        onSubmit={async (data,{resetForm}) => {
                            console.log(data)
                            let resp = await AccountService.createUser(data)
                            if(resp.user){
                                showToast(resp.message,'success')
                                resetForm()
                                nav('/')
                            }else{
                                 showToast(resp.message,'error')
                            }
                            console.log(resp)
                        }}
                    >
                        {({
                            handleSubmit,
                            handleChange,
                            handleBlur,
                            values,
                            touched,
                            errors,
                            isSubmitting,
                        }) => {
                            
                            return (
                                <form onSubmit={handleSubmit}>
                                    <div className="row">
                                        <div className="col-12 mb-3">
                                            <h4>Create Account</h4>
                                        </div>
                                        <div className="col-12 form-group">
                                            <label htmlFor="emp_name">
                                                <b>Employee name</b>
                                            </label>
                                            <input
                                                id="emp_name"
                                                type={`text`}
                                                className="form-control"
                                                name="emp_name"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.emp_name}
                                            />
                                            {errors.emp_name &&
                                                touched.emp_name && (
                                                    <p className="text-danger">
                                                        {errors.emp_name}
                                                    </p>
                                                )}
                                        </div>
                                        <div className="col-12 form-group">
                                            <label htmlFor="job_level">
                                                <b>Job Level</b>
                                            </label>
                                            <select
                                                id="job_level"
                                                className="form-control"
                                                name="job_level"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.job_level}
                                            >
                                                <option defaultValue={``}>
                                                    --select--
                                                </option>
                                                <option value={`supervisor`}>
                                                    Supervisor
                                                </option>
                                                <option value={`junior`}>
                                                    Junior
                                                </option>
                                            </select>
                                            {errors.job_level &&
                                                touched.job_level && (
                                                    <p className="text-danger">
                                                        {errors.job_level}
                                                    </p>
                                                )}
                                        </div>

                                        <div className="col-12 form-group">
                                            <label htmlFor="department">
                                                <b>Department</b>
                                            </label>
                                            <select
                                                name="department"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.department}
                                                id="department"
                                                className="form-control"
                                            >
                                                <option defaultValue={``}>
                                                    --select--
                                                </option>
                                                <option value={`IT department`}>
                                                    IT department
                                                </option>
                                                <option
                                                    value={`P&L department`}
                                                >
                                                    Purchasing and Logistics
                                                </option>
                                            </select>
                                            {errors.department &&
                                                touched.department && (
                                                    <p className="text-danger">
                                                        {errors.department}
                                                    </p>
                                                )}
                                        </div>

                                        <div className="col-12 form-group">
                                            <label htmlFor="email">
                                                <b>Email Address</b>
                                            </label>
                                            <input
                                                name="email"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.email}
                                                type={`email`}
                                                className="form-control"
                                            />
                                            {errors.email &&
                                                touched.email && (
                                                    <p className="text-danger">
                                                        {errors.email}
                                                    </p>
                                                )}
                                        </div>
                                        <div className="col-12 form-group">
                                            <label htmlFor="password">
                                                <b>Password</b>
                                            </label>
                                            <input
                                                name="password"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.password}
                                                type={`password`}
                                                className="form-control"
                                            />
                                            {errors.password &&
                                                touched.password && (
                                                    <p className="text-danger">
                                                        {errors.password}
                                                    </p>
                                                )}
                                        </div>
                                        <div className="col-12 form-group">
                                            <button
                                            disabled={isSubmitting}
                                                type="submit"
                                                className="btn btn-primary w-100"
                                            >
                                               {  isSubmitting ? 'submitting...': 'Sign Up' }
                                            </button>
                                        </div>
                                        <div className="col-12 form-group">
                                            <span></span> Login{" "}
                                            <Link to={`/`}>
                                                Here <ArrowForward />{" "}
                                            </Link>
                                        </div>
                                    </div>
                                </form>
                            );
                        }}
                    </Formik>
                </div>
            </div>
        </div>
    );
};

export default Register;
