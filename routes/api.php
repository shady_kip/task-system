<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['middleware' => ['guest:api']], function () {
    Route::post('/create-user', 'api\auth\RegisterController@createUser');
    Route::post('/login-user', 'api\auth\LoginController@login');
});
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/members/{department}', 'api\DataController@fetchUsers');


    // category routes
    Route::get('/categories', 'api\CategoryController@index');
    Route::post('/categories/create', 'api\CategoryController@create');
    Route::post('/categories/update', 'api\CategoryController@update');
    Route::get('/categories/delete/{slug}', 'api\CategoryController@delete');

    // task routes
    Route::get('/tasks', 'api\TaskController@index');
    Route::post('/tasks/create', 'api\TaskController@store');
    Route::post('/tasks/docs/create', 'api\UploadController@upload');
    Route::post('/tasks/update', 'api\TaskController@update');
    Route::get('/tasks/delete/{slug}', 'api\TaskController@delete');

    Route::get('logout', 'api\AccountController@logout');
});
